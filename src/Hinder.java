import java.awt.Graphics;
import java.io.IOException;

public class Hinder {
	int y;
	int x;
	int b;
	boolean active;
	public static int x1 = 100;
	double DeathByWallTaunt = Math.random() * 100;

	Hinder(int bb) {
		y = (int) (Math.random() * 200);
		x1 = (int) (Math.random() * 250) + x1 + 250;// avst�nd mellan hinderna
		x = x1;
		b = bb;
		active = true;
	}

	public void rita(Graphics g) {
		// g.fillRect(x, y, 25, 180);
		g.drawImage(Spel.wall, x, y, 20, 150, null);
	}
	public void actionPerformed() throws IOException {
		if ((x - 50) <= 0 && active) {
			double DeathSound = Math.random() * 100;
			if (DeathSound >= 50) {
				Sound.play_sound("src/Audio/lvlUp1.au");
			} else {
				Sound.play_sound("src/Audio/lvlUp2.au");
			}
			Spel.lvl = Spel.lvl + 1; // 1
			Spel.score = Spel.score + 1; // 1

			active = false;
		}
		if (Spel.lvl < 10) {
			x = x - 2;
		}
		if (Spel.lvl >= 10 && Spel.lvl < 20) {
			x = x - 2 - 1;
		}
		if (Spel.lvl >= 20 && Spel.lvl < 30) {
			x = x - 2 - 2;
		}
		if (Spel.lvl >= 30 && Spel.lvl < 70) {
			x = x - 2 - 3;
		}
		if (Spel.lvl >= 70 && Spel.lvl < 100) {
			x = x - 2 - 4;
		}
		if (Spel.lvl >= 100 && Spel.lvl < 200) {
			x = x - 2 - 5;
		}
		if (Spel.lvl >= 200) {
			x = x - 2 - 6;
		}
	}

	public void col(int xb, int yb) {
		if (((x + 20) > (60 + 14) && x < (60 + 38) && (y + 150) > (Spel.y + 2) && y < Spel.y + 31) // Hitbox
																									// f�r
																									// Huvudet
				|| ((x + 20) > (60 + 2) && x < (60 + 14) && (y + 150) > (Spel.y + 23) && y < (Spel.y + 37))) { // hitbox
																												// f�r
																												// benen
			if (Spel.lvl >= 35) {
				Sound.play_sound("src/Audio/GameOver1.au");
			}
			if (DeathByWallTaunt >= 90) {
				Sound.play_sound("src/Audio/DeathByWall1.au");
			} else {
				Sound.play_sound("src/Audio/Skadad1.au");
			}
			Spel.tm.setDelay(10000000);
		}

	}
}