public class Shot {
	int x1;
	int y1;
	int x2 = 0;
	int y2 = 0;
	int xFish;
	int yFish;
	int xLazer;
	int yLazer;
	int speedC = 6;
	public static boolean hit;
	public static boolean fish;
	public static boolean stun = false;

	public void moveFish() {
		xFish = xFish + 5;
		if (Spel.lvl == Spel.bossNummer01 && xFish > 800 && xFish < 800 + 100) {
			stun = true;
			Walrus.AudioElectric = 0;
			Walrus.AudioLazer = 0;
		}
	}

	public void moveFriendly() {
		x2 = x2 + 10;
		if (Spel.lvl == Spel.bossNummer02 && (x2 + 55) > 860 && x2 + 25 < 960 && (y2 + 50) > Boss.y
				&& y2 + 35 < Boss.y + 80) { // draw y = y + 35, x + 25
			Boss.hp = Boss.hp - 1;
			Boss.hpbar = Boss.hpbar - 1;
			hit = true;
			// Erik hurt sound
		}
		
		if (Spel.lvl == Spel.bossNummer01 && (x2 + 30) > 800 && x2 < 900 && (y2 + 45) > Walrus.y
				&& y2 < Walrus.y + 65) {
			hit = true;
			// walrus hurt sound
			if (stun == true) {
				Walrus.hp = Walrus.hp - 1;
				Walrus.hpbar = Walrus.hpbar - 1;
			} else {
				Walrus.hp = Walrus.hp - 1;
				Walrus.hpbar = Walrus.hpbar - 1;
			}
		}
	}

	public void moveEnemy() {
		x1 = x1 - 10;
		xLazer = xLazer - 45;
		if (Spel.lvl == Spel.bossNummer02 && (((x1 + 30) > (60 + 14) && x1 < (60 + 38) && (y1 + 46) > (Spel.y + 2)
				&& y1 + 31 < Spel.y + 31) // Hitbox f�r Huvudet
				|| ((x1 + 30) > (60 + 2) && x1 < (60 + 14) && (y1 + 46) > (Spel.y + 23) && y1 + 31 < (Spel.y + 37)))) { // hitbox
																														// f�r
																														// benen
			if (Boss.invEnable == true) {
				Boss.deflect = true;
				Boss.shoot2();
				Boss.gunShot = true;
			}
			if (Boss.invEnable == false) {
				Spel.tm.setDelay(10000000);
				Sound.play_sound("src/Audio/Skadad2.au");
			}
		}
		if (Spel.lvl == Spel.bossNummer01 && (((xLazer + 75) > (60 + 14) && xLazer + 30 < (60 + 38)
				&& (yLazer + 27) > (Spel.y + 2) && yLazer + 10 < Spel.y + 31) // Hitbox
																				// f�r
																				// Huvudet
				|| ((xLazer + 75) > (60 + 2) && xLazer + 30 < (60 + 14) && (yLazer + 27) > (Spel.y + 23)
						&& yLazer + 10 < (Spel.y + 37)))) { // hitbox f�r benen
			// draw y = y + 10, x + 30;
			// Spel.GameOver1.play();
			Sound.play_sound("src/Audio/Skadad2.au");
			Spel.tm.setDelay(10000000);
		}
	}
}