import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Boss {
	static int LazerTimer = 0;
	static int LazerTimer2 = 0;
	static int duration = 0;
	static int duration2;
	static int x = 850;
	static int y = 200;
	static int SX = x;
	static int SY = y;
	static int random;
	public static int shootNumber;
	static boolean yDirection = true;
	public static int hp = 40;
	public static int hpbar = 40;
	// hp �ndras i spel.java
	static List<Shot> shots1 = new ArrayList<>();
	static List<Shot> shots2 = new ArrayList<>();
	static List<Shot> shotLazers = new ArrayList<>();
	static List<Shot> toRemoveLazer = new ArrayList<>();
	public static int xPos = 0;
	public static boolean stop;
	public static int stopTimer = 0;
	public static int gunTimer = 0;
	public static int bossTimer = 0;
	public static int invTimer = 0;
	static int shootTimer = 0;
	static int shootTimer2 = 0;
	static int bossShootTimer = 0;
	static int aiTimer = 0;
	static boolean shootboolean = false;
	public static int tid = 2;
	public static boolean invEnable;
	static Color gold = new Color(255, 215, 0);
	static boolean deflect = false;
	static boolean gunShot;
	static boolean gunShotBoss;
	static boolean drawBossShoot = true;

	public void init() {
	}

	public static void rita(Graphics g) {
		// Hp bar
		if (Spel.lvl == Spel.bossNummer02 && hp > 0) {
			g.drawImage(Spel.bossDb, Boss.x, Boss.y, null);
			g.setColor(Color.BLACK);
			g.fillRect(348, 8, 304, 34);
			g.setColor(Color.WHITE);
			g.fillRect(350, 10, 300, 30);
			g.setColor(Color.RED);
			g.fillRect(350, 10, (15 * (hpbar / 2)), 30);
			// superbar
			g.setColor(Color.BLACK);
			g.fillRect(98, 8, 104, 34);
			g.setColor(Color.WHITE);
			g.fillRect(100, 10, 100, 30);
			if (invTimer <= 250 && tid == -3) {
				g.setColor(Color.RED);
			} else {
				g.setColor(gold);
			}
			g.fillRect(100, 10, (invTimer / 10), 30);
		}
		// Gun
		if ((shootTimer >= 20 || shootTimer2 >= 100) && Spel.lvl == Spel.bossNummer02 && hp > 0) {
			gunShotBoss = true;
			drawBossShoot = true;
		}
		if (drawBossShoot == false && Spel.lvl == Spel.bossNummer02 && hp > 0) {
			g.drawImage(Spel.boss, x, y, null);
		}
		if (drawBossShoot == true && Spel.lvl == Spel.bossNummer02 && hp > 0) {
			g.drawImage(Spel.bossShoot, x, y, null);
		}
		if (Spel.lvl == Spel.bossNummer02 && gunShot == true) {
			if (invEnable == true) {
				if (invTimer <= 250 && tid == -3) {
					g.drawImage(Spel.umbrellaShootWarning, 100, Spel.y + 4, 60, 38, null);
				} else {
					g.drawImage(Spel.umbrellaShoot, 100, Spel.y + 4, 60, 38, null);
				}
			} else {
				g.drawImage(Spel.gunShoot, 98, Spel.y + 4, null);
			}
		}
		if (Spel.lvl == Spel.bossNummer02 && gunShot == false) {
			if (invEnable == true) {
				if (invTimer <= 250 && tid == -3) {
					g.drawImage(Spel.umbrellaWarning, 100, Spel.y + 4, 40, 38, null);
				} else {
					g.drawImage(Spel.umbrella, 100, Spel.y + 4, 40, 38, null);
				}
			} else {
				g.drawImage(Spel.gun, 98, Spel.y + 9, null);
			}

		}
		for (Shot shot1 : shots1) {
			g.drawImage(Spel.bulletEnemy, shot1.x1, shot1.y1 + 31, 30, 15, null);
		}
		for (Shot shot2 : shots2) {
			if (invEnable == true) {
				g.drawImage(Spel.fish, shot2.x2 + 25, shot2.y2 + 40, 30, 15, null);
			} else {
				g.drawImage(Spel.bulletFriendly, shot2.x2 + 25, shot2.y2 + 31, 30, 15, null);
			}
		}
		// text ruta
		if (hp <= 0 && Spel.lvl == Spel.bossNummer02) {
			g.setColor(Color.black);
			g.fillRect(170, 123, 659, 154);
			g.setColor(Color.orange);
			g.fillRect(172, 125, 655, 150);
			g.setColor(Color.black);
			g.setFont(new Font("ArialBlack", Font.BOLD, 70));
			g.drawString("WINNER!", 350, 184);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 50));
			g.drawString("Du har besegrat Elefant-Erik", 183, 229);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 25));
			g.drawString("Space för endless mode", 364, 260);
		}
	}

	// Enemy attack
	public static void shoot1() {
		Shot newShot1 = new Shot();
		newShot1.y1 = y;
		newShot1.x1 = 820;
		shots1.add(newShot1);
	}

	// Friendly bullet
	public static void shoot2() {
		Shot newShot2 = new Shot();
		newShot2.y2 = Spel.y - 26;
		newShot2.x2 = 133;
		shots2.add(newShot2);
	}

	public static void hpBar() {
	}

	public static void actionPerformed() throws IOException {
		// invincible power
		invTimer = invTimer + tid; // tid
		if (invTimer >= 1000) {
			invTimer = 1000;
		}
		if (invEnable == true) {
			tid = -3;
			if (invTimer <= 0) {
				tid = 2;
				invEnable = false;
			}
		}
		if (gunShotBoss == true) {
			bossTimer = bossTimer + 1;
			if (bossTimer >= 30) {
				gunShotBoss = false;
				bossTimer = 0;
			}
		}
		if (gunShot == true) {
			gunTimer = gunTimer + 1;
			if (gunTimer >= 20 && invEnable == true) {
				gunShot = false;
				gunTimer = 0;
			}
			if (gunTimer >= 10 && invEnable == false) {
				gunShot = false;
				gunTimer = 0;
			}
		}

		if (stop == true) {
			stopTimer = stopTimer + 1;
			if (stopTimer >= 25) {
				stop = false;
				stopTimer = 0;
			}
		}
		// power
		if (LazerTimer2 < 5) {
			LazerTimer = LazerTimer + 1;
			LazerTimer2 = LazerTimer / 100;
		}
		if (Shot.stun == true) {
			duration = duration + 1;
			duration2 = duration / 100;
			System.out.println(duration2);
			if (duration2 == 2) {
				Shot.stun = false;
				duration = 0;
				duration2 = 0;
			}
		}
		// movement
		random = (int) (Math.random() * 100);
		if (yDirection == true) {
			y = y + 5;
		} else {
			y = y - 5;
		}
		if (random == 1) {
			yDirection = true;
			aiTimer = 0;
		}
		if (random == 100) {
			yDirection = false;
			aiTimer = 0;
		}
		if (y >= 270) {
			yDirection = false;
		}
		if (y <= 5) {
			yDirection = true;
		}
		
		// AI
		aiTimer = aiTimer + 1;
		if (aiTimer == 30) {
			if (Spel.y >= y) {
				yDirection = true;
			} else {
				yDirection = false;
			}
			aiTimer = 0;
		}
		// shooting
		shootTimer = shootTimer + 1;
		if (shootTimer >= 30) {
			shoot1();
			shootTimer = 0;
		}
		shootTimer2 = shootTimer2 + 1;
		if (shootTimer2 >= 100) {
			shoot1();
			shootTimer2 = 0;
		}
		if (drawBossShoot == true) {
			bossShootTimer = bossShootTimer + 1;
			if (bossShootTimer >= 2) {
				drawBossShoot = false;
			}
		}
		if (hp <= 0) {
			gunShot = false;
		}
		List<Shot> toRemove1 = new ArrayList<>();
		for (Shot shot1 : shots1) {
			shot1.moveEnemy();
			if (shot1.x1 <= -75 || hp <= 0 || deflect == true) {
				toRemove1.add(shot1);
				deflect = false;
			}
		}
		for (Shot shot1 : toRemove1) {
			shots1.remove(shot1);
		}
		toRemove1.clear();
		// hp
		List<Shot> toRemove2 = new ArrayList<>();
		for (Shot shot2 : shots2) {
			shot2.moveFriendly();
			if (shot2.x2 >= 1000 || hp <= 0 || Shot.hit == true) {
				toRemove2.add(shot2);
				Shot.hit = false;
			}
		}
		for (Shot shot2 : toRemove2) {
			shots2.remove(shot2);
		}
		toRemove2.clear();
	}
}