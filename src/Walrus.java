import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Walrus {
	static boolean remove = false;
	static int x = 800;
	static int y = 200;
	static int SX = x;
	static int SY = y;
	static int speed;
	public static int shootNumber;
	static boolean yDirection = true;
	static List<Shot> shots1 = new ArrayList<>();
	static List<Shot> shots2 = new ArrayList<>();
	static List<Shot> shots3 = new ArrayList<>();
	static List<Shot> toRemove1 = new ArrayList<>();
	static List<Shot> toRemove2 = new ArrayList<>();
	static List<Shot> toRemove3 = new ArrayList<>();
	public static int hpbar = 30;
	public static int hp = 30;
	static int duration = 0;
	static int duration2 = 0;
	static int walrusTimer = 0;
	static int walrusTimer2 = 0;
	static int fishTimer = 0;
	static int fishTimer2 = 0;
	static int fireTimer = 0;
	static int stopTimer = 0;
	static int gunTimer = 0;
	static int hastighet = 1;
	static int AudioElectric = 0, AudioLazer = 0;
	static boolean electricEnable;
	static boolean lazerEnable;
	public static boolean fishEnable;
	public static boolean fishStart = true;
	static double ft = 1.6;
	public static boolean stop;
	static int aiTimer = 0;
	static boolean gunShot;
	static boolean gunShotBoss;

	public void init() {
	}

	public static void rita(Graphics g) {
		// Hp bar
		if (Spel.lvl == Spel.bossNummer01 && hp >= 0) {
			g.setColor(Color.BLACK);
			g.fillRect(348, 8, 304, 34);
			g.setColor(Color.WHITE);
			g.fillRect(350, 10, 300, 30);
			g.setColor(Color.RED);
			g.fillRect(350, 10, (10 * hpbar), 30);
			// super bar
			g.setColor(Color.BLACK);
			g.fillRect(98, 8, 104, 34);
			g.setColor(Color.WHITE);
			g.fillRect(100, 10, 100, 30);
			g.setColor(Color.cyan);
			g.fillRect(100, 10, ((10 * fishTimer) / 80), 30);
		}
		// bullet
		for (Shot shot2 : shots2) {
			g.drawImage(Spel.bulletFriendly, shot2.x2 + 25, shot2.y2 + 31, 30, 15, null);
		}
		// walrus laser
		for (Shot shot1 : shots1) {
			g.drawImage(Spel.lazer, shot1.xLazer + 30, shot1.yLazer + 10, 45, 17, null);
		}
		if (Spel.lvl == Spel.bossNummer01 && gunShot == true) {
			g.drawImage(Spel.gunShoot, 98, Spel.y, null);
		}
		if (Spel.lvl == Spel.bossNummer01 && gunShot == false) {
			g.drawImage(Spel.gun, 98, Spel.y + 5, null);
		}
		if (Spel.lvl == Spel.bossNummer01 && hp > 0) {
			g.drawImage(Spel.walrus, x, y, null);
		}
		// super attack
		for (Shot shot3 : shots3) {
			g.drawImage(Spel.ocean, shot3.xFish - 800, 10, null);
		}
		if (gunShot == true) {
			gunTimer = gunTimer + 1;
			if (gunTimer >= 10) {
				gunShot = false;
				gunTimer = 0;
			}
		}
		// text ruta
		if (hp <= 0 && Spel.lvl == Spel.bossNummer01) {
			g.setColor(Color.black);
			g.fillRect(170, 123, 659, 154);
			g.setColor(Color.CYAN);
			g.fillRect(172, 125, 655, 150);
			g.setColor(Color.black);
			g.setFont(new Font("ArialBlack", Font.BOLD, 50));
			g.drawString("Tjockt med nice!", 270, 175);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 45));
			g.drawString("Du har besegrat Cyborg-Pär", 190, 225);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 25));
			g.drawString("Space för att fortsätta", 390, 260);
		}
		// Walrus
		fireTimer = fireTimer + 1;
		if (fireTimer >= 210 / ft) {
			fireTimer = 0;
		}
		if (electricEnable == true && lazerEnable == false) {
			if (fireTimer <= 10 / ft) {
				Spel.walrus = Spel.walrusElectric01;
			}
			if (fireTimer >= 110 / ft) {
				Spel.walrus = Spel.walrusElectric02;
			}
		}
		if (electricEnable == false && lazerEnable == false) {
			if (fireTimer <= 10 / ft) {
				Spel.walrus = Spel.walrus01;
			}
			if (fireTimer >= 110 / ft) {
				Spel.walrus = Spel.walrus02;
			}
		}
		if (lazerEnable == true) {
			if (fireTimer <= 10 / ft) {
				Spel.walrus = Spel.walrusLazer01;
			}
			if (fireTimer >= 110 / ft) {
				Spel.walrus = Spel.walrusLazer02;
			}
		}
		if (Shot.stun == true) {
			if (fireTimer <= 10 / ft) {
				Spel.walrus = Spel.walrusStunned01;
			}
			if (fireTimer >= 110 / ft) {
				Spel.walrus = Spel.walrusStunned02;
			}
		}
	}

	// Enemy attack
	public static void shoot1() {
		Shot newShot1 = new Shot();
		newShot1.yLazer = y;
		newShot1.xLazer = x - 30;
		shots1.add(newShot1);
	}

	// Friendly bullet
	public static void shoot2() {
		Shot newShot2 = new Shot();
		newShot2.y2 = Spel.y - 26;
		newShot2.x2 = 90 + Spel.diameterB;
		shots2.add(newShot2);
	}

	// Super attack
	public static void shoot3() {
		Shot newShot3 = new Shot();
		newShot3.yFish = 0;
		newShot3.xFish = 0;
		shots3.add(newShot3);
	}

	public static void actionPerformed() throws IOException {
		// walrus
		if (AudioElectric == 1) {
			Sound.play_sound("src/Audio/LaserLaddar.au");
		}
		if (stop == true) {
			stopTimer = stopTimer + 1;
			if (stopTimer >= 25) {
				stop = false;
				stopTimer = 0;
			}
		}

		if (fishEnable == true) {
			shoot3();
			fishEnable = false;
		}
		if (walrusTimer2 <= 8 && Spel.lvl == Spel.bossNummer01 && hp > 0) {
			walrusTimer = walrusTimer + 1;
			walrusTimer2 = walrusTimer / 75;
			if (walrusTimer2 == 2) {
				speed = 0;
				electricEnable = true;
				AudioElectric = AudioElectric + 1;
			}
			if (walrusTimer2 == 3) {
				electricEnable = false;
				AudioElectric = 0;
				lazerEnable = true;
				AudioLazer = AudioLazer + 1;
				shoot1();
				speed = 1;
			}
			if (walrusTimer2 == 4) {
				lazerEnable = false;
				AudioLazer = 0;
			}
			if (walrusTimer2 == 5) {
				speed = 5;
				walrusTimer = 0;
				walrusTimer2 = 0;
			}

		}
		if (AudioLazer == 1) {
			Sound.play_sound("src/Audio/Laser.au");
		}
		if (AudioElectric == 1) {
			Sound.play_sound("src/Audio/LaserLaddar.au");
		}
		if (Shot.stun == true) {
			duration = duration + 1;
			duration2 = duration / 100;
			fishTimer = 0;
			walrusTimer = 0;
			walrusTimer2 = 0;
			electricEnable = false;
			lazerEnable = false;
			if (duration2 == 2) { // 3
				Shot.stun = false;
				duration = 0;
				duration2 = 0;
				fishTimer = 0;
				walrusTimer = 0;
				fishStart = true;
			}
		}
		// movement
		if (fishTimer2 < 8 && fishStart == true) { // 12
			fishTimer = fishTimer + 1;
			fishTimer2 = fishTimer / 100;
		}
		aiTimer = aiTimer + 1;
		if (aiTimer == 30) {
			if (Spel.y >= y) {
				yDirection = true;
			} else {
				yDirection = false;
			}
			aiTimer = 0;
		}
		if (yDirection == true) {
			y = y + speed;
		} else {
			y = y - speed;
		}
		if (y >= 295) {
			yDirection = false;
		}
		if (y <= 5) {
			yDirection = true;
		}
		if (Shot.stun == true) {
			speed = 0;
		}
		if (Shot.stun == false) {
			speed = 5;
		}
		if (hp <= 0) {
			gunShot = false;
		}
		for (Shot shot1 : shots1) {
			shot1.moveEnemy();
			if (shot1.x1 < -205 || hp <= 0) {
				toRemove1.add(shot1);
			}
		}
		for (Shot shot1 : toRemove1) {
			shots1.remove(shot1);
		}
		toRemove1.clear();
		for (Shot shot2 : shots2) {
			shot2.moveFriendly();
			if (shot2.x2 > 1000 || hp <= 0 || Shot.hit == true) {
				toRemove2.add(shot2);
				Shot.hit = false;
			}
		}
		for (Shot shot2 : toRemove2) {
			shots2.remove(shot2);
		}
		toRemove2.clear();
		for (Shot shot3 : shots3) {
			shot3.moveFish();
			if (shot3.xFish > 2000 || hp <= 0) {
				toRemove3.add(shot3);
			}
		}
		for (Shot shot3 : toRemove3) {
			shots3.remove(shot3);
		}
		toRemove3.clear();
	}
}