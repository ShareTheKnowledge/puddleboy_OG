import sun.audio.*;
import java.io.*;

@SuppressWarnings("restriction")
public class Sound {

	public static void play_loop(String MUSIC_FILE) {
		AudioStream backgroundMusic;
		AudioData musicData;
		AudioPlayer musicPlayer = AudioPlayer.player;
		ContinuousAudioDataStream loop = null;

		try {
			backgroundMusic = new AudioStream(new FileInputStream(MUSIC_FILE));
			musicData = backgroundMusic.getData();
			loop = new ContinuousAudioDataStream(musicData);
			musicPlayer.start(loop);
			if (Spel.gameover == true) {
				musicPlayer.stop(loop);
			}
		} catch (IOException error) {
			System.out.println(error);
		}
	}

	public static void play_sound(String SOUND_FILE) {
		try {
			AudioStream audioStream = new AudioStream(new FileInputStream(SOUND_FILE));
			AudioPlayer.player.start(audioStream);
		} catch (Exception error) {
			System.out.println(error);
		}
	}
}