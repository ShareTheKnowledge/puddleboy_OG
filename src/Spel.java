import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.Timer;

public class Spel extends Frame implements ActionListener, KeyListener {
	private static final long serialVersionUID = 1L;
	Hinder h[];
	static int y = 100;
	public BufferedImage bulletC;
	// public static AudioClip ShotAudio, Laser, LaserLaddar, lvlUp1,
	// DeathByWall1, GameOver1, LevelUp1, lvlUp2,
	// Skadad1, Skadad2, WalRus2, Vind1, Vind2, Vind3, ParaplyLjud, SuperVag,
	// EEMeloner;
	public static BufferedImage walrusElectric, walrus, walrus01, walrus02, walrusStunned01, walrusStunned02,
			walrusElectric01, walrusElectric02, walrusLazer01, walrusLazer02, dyrone, spelare, background01,
			background02, background03, background04, backgroundF, backgroundA, wall, boss, Fullbar, Emptybar,
			spelareNer, spelareInvNer, spelareInv, gun, bulletFriendly, bulletEnemy, bossShoot, bossDb, chicken,
			gunShoot, ocean, ocean2, fish, lazer, umbrella, umbrellaWarning, umbrellaShoot, umbrellaShootWarning;
	public static int diameterB = 43;
	public static int diameterH = 39;
	public static int BgX1, BgX2;
	static Timer tm;
	boolean upp;
	boolean var = true;
	public int hinderC = 10;
	public static int bossNummer01 = 10;
	static int bossNummer02 = 30;
	// hinderC till 1 p� bossnummer01 i resetGame()
	public static int lvl = 0;
	public static int score = 0;
	static int highscore;
	public static Color colorHs;
	public static boolean alive;
	static boolean startScreen = true;
	int textTimer = 0;
	static boolean gameover = false;
	double deathCount = 0;
	int bossTimer01 = 0;
	int bossTimer02 = 0;
	int GoingDown = 0;
	boolean Boss01 = false;
	boolean Boss02 = false;

	public static void main(String args[]) {
		new Spel();
	}

	public Spel() {
		setLayout(new FlowLayout());
		setSize(1000, 400);
		// setBounds(-200, -100, 1000, 400);
		setUndecorated(true);
		setLocationRelativeTo(null);
		setTitle("PuddleBoy the Game!");
		// setLocation(3000,100);

		setVisible(true);

		this.addKeyListener(this);

		// Dessa ska egentligen pausas när man dör, men lyckas inte stänga av dom!
		Sound.play_loop("src/Audio/Vind1.au");
		Sound.play_loop("src/Audio/Vind2.au");
		Sound.play_loop("src/Audio/Vind3.au");

		// Audio
		/*
		 * ParaplyLjud = getAudioClip(getCodeBase(), "Audio/ParaplyLjud.au"); SuperVag =
		 * getAudioClip(getCodeBase(), "Audio/SuperV�g.au"); EEMeloner =
		 * getAudioClip(getCodeBase(), "Audio/EEMeloner.au"); lvlUp1 =
		 * getAudioClip(getCodeBase(), "Audio/lvlUp1.au"); lvlUp2 =
		 * getAudioClip(getCodeBase(), "Audio/lvlUp2.au"); ShotAudio =
		 * getAudioClip(getCodeBase(), "Audio/ShotAudio.au"); Laser =
		 * getAudioClip(getCodeBase(), "Audio/Laser.au"); LaserLaddar =
		 * getAudioClip(getCodeBase(), "Audio/LaserLaddar.au"); DeathByWall1 =
		 * getAudioClip(getCodeBase(), "Audio/DeathByWall1.au"); GameOver1 =
		 * getAudioClip(getCodeBase(), "Audio/GameOver1.au"); LevelUp1 =
		 * getAudioClip(getCodeBase(), "Audio/LevelUp1.au"); Skadad1 =
		 * getAudioClip(getCodeBase(), "Audio/Skadad1.au"); Skadad2 =
		 * getAudioClip(getCodeBase(), "Audio/Skadad2.au"); WalRus2 =
		 * getAudioClip(getCodeBase(), "Audio/WalRus2.au"); Vind1 =
		 * getAudioClip(getCodeBase(), "Audio/Vind1.au"); Vind2 =
		 * getAudioClip(getCodeBase(), "Audio/Vind2.au"); Vind3 =
		 * getAudioClip(getCodeBase(), "Audio/Vind3.au");
		 */
		// All images
		try {
			background01 = ImageIO.read(this.getClass().getResource("Sprites/background01.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			background02 = ImageIO.read(this.getClass().getResource("Sprites/background02.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			background03 = ImageIO.read(this.getClass().getResource("Sprites/background03.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			spelare = ImageIO.read(this.getClass().getResource("Sprites/spelare.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			spelareNer = ImageIO.read(this.getClass().getResource("Sprites/spelareNer.png"));
		} catch (IOException e4) {

			e4.printStackTrace();
		}
		try {
			spelareInv = ImageIO.read(this.getClass().getResource("Sprites/spelareInv.png"));
		} catch (IOException e3) {

			e3.printStackTrace();
		}
		try {
			spelareInvNer = ImageIO.read(this.getClass().getResource("Sprites/spelareInvNer.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			gun = ImageIO.read(this.getClass().getResource("Sprites/gun.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			gunShoot = ImageIO.read(this.getClass().getResource("Sprites/gunShoot.png"));
		} catch (IOException e3) {

			e3.printStackTrace();
		}
		try {
			bulletFriendly = ImageIO.read(this.getClass().getResource("Sprites/bullet.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			bulletEnemy = ImageIO.read(this.getClass().getResource("Sprites/bulletEnemy.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			lazer = ImageIO.read(this.getClass().getResource("Sprites/lazer.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			ocean = ImageIO.read(this.getClass().getResource("Sprites/ocean.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			wall = ImageIO.read(this.getClass().getResource("Sprites/wall.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			boss = ImageIO.read(this.getClass().getResource("Sprites/boss.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			bossShoot = ImageIO.read(this.getClass().getResource("Sprites/bossShoot.png"));
		} catch (IOException e) {

			e.printStackTrace();
		}
		try {
			umbrella = ImageIO.read(this.getClass().getResource("Sprites/umbrella.png"));
		} catch (IOException e3) {

			e3.printStackTrace();
		}
		try {
			umbrellaWarning = ImageIO.read(this.getClass().getResource("Sprites/umbrellaWarning.png"));
		} catch (IOException e7) {

			e7.printStackTrace();
		}
		try {
			umbrellaShoot = ImageIO.read(this.getClass().getResource("Sprites/umbrellaShoot.png"));
		} catch (IOException e7) {
			e7.printStackTrace();
		}
		try {
			umbrellaShootWarning = ImageIO.read(this.getClass().getResource("Sprites/umbrellaShootWarning.png"));
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			fish = ImageIO.read(this.getClass().getResource("Sprites/fish.png"));
		} catch (IOException e7) {

			e7.printStackTrace();
		}
		try {
			walrus01 = ImageIO.read(this.getClass().getResource("Sprites/walrus01.png"));
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		try {
			walrus02 = ImageIO.read(this.getClass().getResource("Sprites/walrus02.png"));
		} catch (IOException e6) {

			e6.printStackTrace();
		}
		try {
			walrusStunned01 = ImageIO.read(this.getClass().getResource("Sprites/walrusStunned01.png"));
		} catch (IOException e5) {

			e5.printStackTrace();
		}
		try {
			walrusStunned02 = ImageIO.read(this.getClass().getResource("Sprites/walrusStunned02.png"));
		} catch (IOException e5) {

			e5.printStackTrace();
		}
		try {
			walrusLazer01 = ImageIO.read(this.getClass().getResource("Sprites/walrusLazer01.png"));
		} catch (IOException e4) {

			e4.printStackTrace();
		}
		try {
			walrusLazer02 = ImageIO.read(this.getClass().getResource("Sprites/walrusLazer02.png"));
		} catch (IOException e3) {

			e3.printStackTrace();
		}
		try {
			walrusElectric01 = ImageIO.read(this.getClass().getResource("Sprites/walrusElectric01.png"));
		} catch (IOException e2) {

			e2.printStackTrace();
		}
		try {
			walrusElectric02 = ImageIO.read(this.getClass().getResource("Sprites/walrusElectric02.png"));
		} catch (IOException e1) {

			e1.printStackTrace();
		}
		try {
			List<String> lines = Files.readAllLines(Paths.get("src/highscore/highscore.txt"),
					Charset.defaultCharset());
			for (String line : lines) {
				highscore = java.lang.Integer.parseInt(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		startScreen = true;
		resetGame();
	}

	public void resetGame() {
		// Insert vind ljud.
		if (Boss01 == true) {
			hinderC = 1;
			bossNummer01 = 1;
			lvl = 0;
			score = 0;
		}
		if (Boss02 == true) {
			hinderC = 1;
			bossNummer02 = 1;
			lvl = 0;
			score = 0;
		}
		if ((lvl == bossNummer01 && Walrus.hp <= 0) || (lvl == bossNummer02 && Boss.hp <= 0)) {
			lvl = lvl + 5;
			score = score + 5;
			Sound.play_sound("src/Audio/LevelUp1.au");
		}
		boolean easymode = true;
		if (Boss01 == false && Boss02 == false) {
			if (easymode == true) {
				if ((lvl < bossNummer01)) { // Start
					lvl = 0;
					score = 0;
					hinderC = 10;
				}
				if ((lvl == bossNummer01 && Walrus.hp > 0)) { // Boss 1
					lvl = bossNummer01 - 1;
					score = 0;
					hinderC = 1;
				}
				if ((lvl > bossNummer01 && lvl < bossNummer02)) { // Efter Boss 1
					lvl = bossNummer01 + 5;
					score = score + 5;
					hinderC = 15;
				}
				if (lvl == bossNummer02 && Boss.hp > 0) { // Boss 2
					lvl = bossNummer02 - 1;
					score = 0;
					hinderC = 1;
				}
				if ((lvl > bossNummer02 + 5)) { // Efter boss 2
					lvl = 0;
					score = 0;
				}
			} else {
				if ((lvl <= bossNummer01 || lvl > bossNummer02 + 5)) {
					lvl = 0;
					score = 0;
				}
				if (lvl > bossNummer01 && lvl < bossNummer02) {
					lvl = 0;
					hinderC = 15;
					score = 0;
					// 15
				}
				if (lvl < bossNummer01 && Boss01 == false && Boss02 == false) {
					lvl = 0;
					hinderC = 10;
					score = 0;
					// 10, byt f�r att �ndra antal av f�rsta "set" hinder
				}
			}
		}
		if ((lvl > bossNummer02)) { // Efter boss 2
			hinderC = 300;
		}
		if (lvl <= bossNummer01) {
			backgroundA = background01;
			backgroundF = background01;
		}
		if (lvl > bossNummer01 && lvl < bossNummer02) {
			backgroundA = background02;
			backgroundF = background02;
		}

		h = new Hinder[hinderC];
		for (int i = 0; i < hinderC; i++) {
			h[i] = new Hinder(this.getWidth());
			tm = new Timer(10, this);
		}
		Boss.hp = 40;
		Boss.hpbar = 40;
		Boss.invTimer = 0;
		Boss.invEnable = false;
		Boss.gunShot = false;
		Boss.shots1.clear();
		Boss.shots2.clear();
		Walrus.hp = 30;
		Walrus.hpbar = 30;
		Walrus.shots1.clear();
		Walrus.shots2.clear();
		Walrus.shots3.clear();
		Walrus.gunShot = false;
		Walrus.walrusTimer = 0;
		Walrus.walrusTimer2 = 0;
		Walrus.fishTimer = 0;
		Walrus.fishStart = true;
		Walrus.lazerEnable = false;
		Walrus.electricEnable = false;
		Shot.stun = false;
		gameover = false;
		Hinder.x1 = 100;
		y = 100;
	}

	public void paint(Graphics g) {
		this.requestFocus();
		if (startScreen == true) {
			g.drawImage(background01, 0, 0, 1000, 400, this);
			g.setColor(Color.black);
			g.fillRect(170, 123, 659, 154);
			g.setColor(Color.orange);
			g.fillRect(172, 125, 655, 150);
			g.setColor(Color.black);
			g.setFont(new Font("ArialBlack", Font.BOLD, 50));
			g.drawString("Upp pil för att flyga", 230, 175);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 28)); // 34
			g.drawString("A = superattack, D = skjuta, Ner pil = dyka", 190, 220);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 25));
			g.drawString("Space för att starta", 390, 260);
			g.setFont(new Font("ArialBlack", Font.PLAIN, 15));
			g.drawString("0 = reset highscore", 660, 250);
			g.drawString("esc = exit", 660, 270);
			repaint();
		} else {
			try {
				BgX2 = BgX1 + 995;
				BgX1 = (h[0].x - 1995) % 1000;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			g.drawImage(backgroundF, BgX1, 0, (int) getBounds().getWidth(), (int) getBounds().getHeight(), this);
			g.drawImage(backgroundA, BgX2, 0, (int) getBounds().getWidth(), (int) getBounds().getHeight(), this);
			g.fillRect(0, 351, 1350, 1);
			if (Boss.invEnable == true) {
				if (upp == true) {
					g.drawImage(spelareInv, 60, y, diameterB, diameterH, this);
				} else {
					g.drawImage(spelareInvNer, 60, y, diameterB, diameterH, this);
				}
			} else {
				if (upp == true) {
					g.drawImage(spelare, 60, y, diameterB, diameterH, this);
				} else {
					g.drawImage(spelareNer, 60, y, diameterB, diameterH, this);
				}
			}
			g.fillRect(0, 0, 1350, 1);
			g.setColor(colorHs);
			g.drawString("highscore: " + highscore, 900, 20);
			for (int i = 0; i < hinderC; i++) {
				try {
					h[i].rita(g);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				g.setColor(Color.black);
				g.drawString("score: " + score, 900, 35);
			}
			Boss.rita(g);
			Walrus.rita(g);
			if (gameover == true) {
				Walrus.AudioElectric = 0;
				Walrus.AudioLazer = 0;
				g.setColor(Color.black);
				g.fillRect(170, 123, 659, 154);
				g.setColor(Color.orange);
				g.fillRect(172, 125, 655, 150);
				g.setColor(Color.black);
				g.setFont(new Font("ArialBlack", Font.BOLD, 50));
				g.drawString("GAME OVER", (int) (340), (int) (175));
				g.setFont(new Font("ArialBlack", Font.PLAIN, 45));
				if (deathCount == 1) {
					g.drawString("Du har dött " + (int) (deathCount) + " gång", (int) (290), (int) (225));
				} else {
					g.drawString("Du har dött " + (int) (deathCount) + " gånger", (int) (290), (int) (225));
				}
				g.setFont(new Font("ArialBlack", Font.PLAIN, 25));
				g.drawString("Space för att starta om", (int) (370), (int) (260));

				g.setFont(new Font("ArialBlack", Font.PLAIN, 18));
				g.drawString("LvL: " + lvl, (int) 180, (int) 265);
				textTimer = textTimer + 1;
				// TODO
				// Stäng av ljudet från loop.
				// Vind1.stop();
				// Vind2.stop();
				// Vind3.stop();
			} else {
				textTimer = 0;
			}
			repaint();

		}
	}

	public void update(Graphics g) {
		Graphics offgc;
		Image offscreen = null;
		Dimension d = getSize();
		offscreen = createImage(d.width, d.height);
		offgc = offscreen.getGraphics();
		offgc.setColor(getBackground());
		offgc.fillRect(0, 0, d.width, d.height);
		offgc.setColor(getForeground());
		paint(offgc);
		g.drawImage(offscreen, 0, 0, this);
	}


	public void actionPerformed(ActionEvent e) {
		if (score > highscore) {
			highscore = score;
			colorHs = Color.RED;
			try {
				Files.write(Paths.get("src/highscore/highscore.txt"), (highscore + "").getBytes(),
						StandardOpenOption.TRUNCATE_EXISTING);
			} catch (Exception ex) {
				try {
					Files.write(Paths.get("src/highscore/highscore.txt"), (highscore + "").getBytes(),
							StandardOpenOption.CREATE);
				} catch (Exception ex1) {
					System.out.println("Failed to write highscore");
					ex1.printStackTrace();
				}
			}
		}
		if (tm.getDelay() == (10000000) && alive == false) {
			deathCount = deathCount + 1;
			gameover = true;
		}

		if (var == true) {
			y = y + (4 + GoingDown);
			upp = false;
		}
		if (var == false) {
			y = y - 4;
			upp = true;
		}

		if (y <= -2) {
			tm.setDelay(10000000);
		}
		if (y >= 351 - 33) {
			tm.setDelay(10000000);
		}
		for (int i = 0; i < hinderC; i++) {
			try {
				h[i].actionPerformed();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			h[i].col(60, y);
		}
		if (Boss.hp <= 0 || Walrus.hp <= 0) {
			tm.setDelay(10000000);
			alive = true;
		} else {
			alive = false;
		}
		if (lvl >= bossNummer02) {
			backgroundA = background03;
			backgroundF = background03;
		}
		if (lvl == bossNummer02 && Boss.hp > 0) {
			try {
				Boss.actionPerformed();
			} catch (IOException e1) {

				e1.printStackTrace();
			}
		}
		if (lvl >= bossNummer01 && lvl < bossNummer02) {
			backgroundA = background02;
			backgroundF = background02;
		}
		if (lvl == bossNummer01 && Walrus.hp > 0) {
			try {
				Walrus.actionPerformed();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		repaint();
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == 38) {
			var = false;
		}
		if (e.getKeyCode() == 40) {

			GoingDown = 4;
		}
		if (e.getKeyCode() == 32) {
			startScreen = false;
		}
		if (e.getKeyCode() == 38 && startScreen == false) {
			tm.start();
		}
		if (e.getKeyCode() == 27) {
			System.exit(0);
		}
		if (lvl == bossNummer02 && e.getKeyCode() == 68 && Boss.stop == false && Boss.invEnable == false) {
			Sound.play_sound("src/Audio/ShotAudio.au");
			Boss.shoot2();
			Boss.gunShot = true;
			Boss.stop = true;
		}
		if (lvl == bossNummer01 && e.getKeyCode() == 68 && Walrus.stop == false) {
			Sound.play_sound("src/Audio/ShotAudio.au");
			Walrus.shoot2();
			Walrus.gunShot = true;
			Walrus.stop = true;
		}
		if (e.getKeyCode() == 65 && lvl == bossNummer01 && Walrus.fishTimer2 >= 8) { // 12
			Walrus.fishTimer2 = 0;
			Walrus.fishTimer = 0;
			Walrus.fishEnable = true;
			Walrus.fishTimer = 10;
			Walrus.fishStart = false;
		}
		if (e.getKeyCode() == 65 && lvl == bossNummer02 && Boss.invTimer >= 1000) {
			Boss.invEnable = true;
		}
		if ((lvl != bossNummer01 || Walrus.hp > 0) && (lvl != bossNummer02 || Boss.hp > 0)) {
			if (tm.getDelay() == (10000000) && e.getKeyCode() == 32) {
				resetGame();
			}
		}
		if (lvl == bossNummer01 && Walrus.hp <= 0) {
			if (tm.getDelay() == (10000000) && e.getKeyCode() == 32) {
				resetGame();
			}
		}
		if (lvl == bossNummer02 && Boss.hp <= 0) {
			if (tm.getDelay() == (10000000) && e.getKeyCode() == 32) {
				resetGame();
			}
		}
		if (e.getKeyCode() == 49 && startScreen == true) {
			Boss01 = true;
			resetGame();
		}
		if (e.getKeyCode() == 50 && startScreen == true) {
			Boss02 = true;
			resetGame();
		}
		if (e.getKeyCode() == 48 && startScreen == true) {
			highscore = 0;
		}
		if (e.getKeyCode() == 37) {
			Spel.tm.setDelay(1000000);
		}
		if (e.getKeyCode() == 39) {
			resetGame();
		}
		repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == 38) {
			var = true;
		}
		if (e.getKeyCode() == 40) {
			GoingDown = 0;
		}
	}
}
